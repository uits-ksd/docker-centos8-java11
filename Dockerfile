# Built on Base CentOS 8 image with updates and default packages
# Adding Java11 OpenJDK Runtime Environment

FROM centos:centos8
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

# Install essential packages and do some cleanup; modified from https://bitbucket.org/ua-ecs/docker_dokuwiki/src/master/Dockerfile
RUN \
    yum -y install epel-release && \
    yum clean all && yum makecache && yum -y update && \
    yum -y install less wget httpd httpd-tools unzip \
                   sudo nfs-utils cronie rsyslog && \
    yum clean all && \
    rm -rf /var/lib/apt/lists/*

# Install AWS CLI; instructions from https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Modify default rsyslog.conf to set it to run the right module types for local logging, update security for cron, and create the cron log file
# For running in a container; source is https://bitbucket.org/ua-ecs/docker_dokuwiki/src/master/Dockerfile
# Changes to sed commands reflect new format; see https://www.rsyslog.com/doc/v8-stable/configuration/converting_to_new_format.html
RUN \
    sed -i -e 's/^\       SysSock.Use="off"/\       SysSock.Use="on"/' \
           -e 's/^\module(load="imjournal/\#\module(load="imjournal/' \
           -e 's/^\       StateFile="imjournal.state/\#\       StateFile="imjournal.state/' \
           -e 's/^\#\module(load="imklog")/\module(load="imklog")/' \
           -e 's/^\#\module(load"immark")/\module(load="immark")/' \
           /etc/rsyslog.conf && \
    sed -i '/session.*required.*pam_loginuid.so/d' /etc/pam.d/crond && \
    touch /var/log/cron

# copy in local testing and make executable
COPY bin /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Install Java 11
RUN yum -y install java-11-openjdk
