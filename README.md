# University of Arizona CentOS 8 and Java 11 Docker Image

---

This repository is for the Kuali team's Java 11 image based on official centos 8 Docker image hosted on Docker Hub.

**NOTE: As of December 31, 2021, CentOS 8 reached EOL. This repository has been replaced by https://bitbucket.org/uits-ksd/docker-centos7-java11/**

### Description
This project defines an image that includes CentOS 8 and Java 11.

### Requirements
This is based on a **centos8** tagged image from https://hub.docker.com/_/centos. According to their documentation: 
> The CentOS Project offers regularly updated images for all active releases. These images will be updated monthly or as needed for emergency fixes. These rolling updates are tagged with the major version number only. 

Related work for utilizing this image for KFS revealed that additional packages are required to be installed and started explicitly. Code was copied from https://bitbucket.org/ua-ecs/docker_dokuwiki/src/master/Dockerfile, which is using the **centos7** Docker image.

### Building
#### Locally
If you are testing changes and building the image locally, run this command to build a *java11* Docker image, replacing $BASE_IMAGE_TAG_DATE with the current date: `docker build -t kuali/tomcat8:java11-ua-release-$BASE_IMAGE_TAG_DATE .` This will create a Docker image in your local with the tag of java11-ua-release-2020-06-04, for example.

#### Jenkins
The build command we effectively use is: `docker build -t kuali/tomcat8:java11-ua-release-<DATE> --force-rm .`

We then tag with the build date and push the image to AWS with these commands: 
```
docker tag ${DOCKER_REPOSITORY_NAME}:${TAG} ${DOCKER_REPOSITORY_URI}:${TAG}
docker push ${DOCKER_REPOSITORY_URI}:${TAG}
```

Example of a resulting tag: _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat8:java11-ua-release-2020-06-04_.

Jenkins link: https://kfs-jenkins.ua-uits-kuali-nonprod.arizona.edu/job/Development/view/Docker%20Baseline/

*Caveat:* A new docker-centos8-java11-tomcat8 image will need to be updated if a new *java11* image is created.

### Local Testing
To test changes to the Dockerfile:
1. Add this to the end: `ENTRYPOINT /usr/local/bin/local-testing.sh`
2. Use a command like this after you've successfully created a local test image, substituting the BASE_IMAGE_TAG_DATE you used in the build command: `docker run -d --name=java11 --privileged=true kuali/tomcat8:java11-ua-release-$BASE_IMAGE_TAG_DATE .`
3. Log into the running container: `docker exec -it java11 /bin/bash`
4. Make sure to remove the ENTRYPOINT instruction added to the Dockerfile for testing before you commit your changes.